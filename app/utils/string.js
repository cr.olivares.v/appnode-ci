function saluda(nombre) {
    return 'Hola ' + nombre;
}

function despidete(nombre) {
    return 'Adios ' + nombre;
}

module.exports = {
    saluda,
    despidete
}