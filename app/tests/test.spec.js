const {saluda,despidete} = require('../utils/string')

test('Saludando a Cristian', () => {
    expect(saluda('Cristian')).toBe('Hola Cristian');
});


test('Despidete de Cristian', () => {
    expect(despidete('Cristian')).toBe('Adios Cristian');
});