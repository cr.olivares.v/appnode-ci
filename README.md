# appnode-ci

Aplicación de prueba para implementar CI con gitlab-ci

El propósito de este repo es crear una aplicación con NodeJS básica con TDD (Test unitarios), esta aplicación una vez que subio los cambios a gitlab (commit-push) y se fusione su rama a la rama master, ejecutará automaticamente un pipeline con gitlab-ci que probará que todos los test unitarios que se hayan generado con éxito, para ese propositio utilizaremos Jest (aunque puede ser Mocha o cualquiera de su gusto), una vez que las pruebas fueron aceptadas se generará la imagen de docker y subirá los cambios a docker-hub con el numero de tag segun el hash de gitlab

grkfglgmlkrtmkltrlmmtlmrttr
