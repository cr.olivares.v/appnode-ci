var express = require('express');

const Sentry = require('@sentry/node');
const Tracing = require("@sentry/tracing");

var app = express();

Sentry.init({
  dsn: "https://b2ec08e831b440a78cc7ac99d2fda7a9@o1009523.ingest.sentry.io/5973703",
  integrations: [
    new Sentry.Integrations.Http({ tracing: true }),
    new Tracing.Integrations.Express({ app }),
  ],
  tracesSampleRate: 1.0,
});

app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());

var root = {
    msg: "Prueba de NodeJS 3.0",
};
app.get('/', function rootHandler(req, res) {
  res.send(root);
});

app.get('/public', function rootHandler(req, res) {
  res.send('Endpoint Public');
});

app.get('/private', function rootHandler(req, res) {
  res.send('Endpoint Private');
});

app.use(Sentry.Handlers.errorHandler());
app.use(function onError(err, req, res, next) {
  res.statusCode = 500;
  res.end(res.sentry + "\n");
});

app.listen(3000, '0.0.0.0', function () {
  console.log('Node Corriendo en el puerto 3000!');
});  